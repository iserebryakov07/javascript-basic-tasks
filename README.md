# JavaScript Basic Tasks

## Tasks

#### Deaf Phone Game

You're playing a game called Deaf Phone.
Your task is to get the data which computer tells you and reproduce it completely. 
For example:
```js
deafPhoneGame('Ouagadougou is the capital of Burkina Faso') // returns 'Ouagadougou is the capital of Burkina Faso'
deafPhoneGame({ topSecretHash: 'f2jjJ()F@#()d@', topSecretNumber: -312 }) // returns { topSecretHash: 'fj23fji23fji23f', topSecretNumber: -312 }
```

Write your code in `src/index.js` within an appropriate function `deafPhoneGame`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Name Hockey Game Winner

Your tasks is to create a title for a some News Portal with an info about the result of the latest hocket match.
For example:
```js
nameHockeyGameWinner('Bolts', '5:4', 'Capitals') // returns 'Bolts defeat Capitals with the score 5:4'
nameHockeyGameWinner('Stars', '6:7OT', 'Flames') // returns 'Flames defeat Stars in OT with the score 7:6OT'
```
Write your code in `src/index.js` within an appropriate function `nameHockeyGameWinner`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Swap Two Elements in an Array
Your task is to swap two elements in a provided array and elements indexes.
If it's not possible, return the given array. For example
```js
swapTwoElementsInArray([65, 12, 3], 0, 2) // returns [3, 12, 65]
swapTwoElementsInArray([3, 4, 65, 12, -1], -100, 4) // returns [3, 4, 65, 12, -1]
```
Write your code in `src/index.js` within an appropriate function `nameHockeyGameWinner`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Balanced Brackets
Your tasks is to implement a function which return true if the specified string has the balanced brackets and false otherwise.
Balanced means that is, whether it consists entirely of pairs of opening/closing brackets
(in that order), none of which mis-nest.
Brackets include [],(),{},<>
For example
```js
isBracketsBalanced('[[][]]') // returns true
isBracketsBalanced('({}[]<>(((())))') // return false
```
Write your code in `src/index.js` within an appropriate function `isBracketsBalanced`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Memoize
Your tasks is to create a Memoize function. Memoizes passed function and returns function which invoked first time calls the passed function and then always returns cached result.
For example
```js
const memoizer = memoize(() => Math.random());
memoizer() => some random number  (first run, evaluates the result of Math.random())
memoizer() => the same random number  (second run, returns the previous cached result)
memoizer() => the same random number  (next run, returns the previous cached result)
```
Write your code in `src/index.js` within an appropriate function `memoize`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: javascript-basic-tasks
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-basic-tasks/  
4. Go to folder `javascript-basic-tasks`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-basic-tasks)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
